package martin.kompan.demo.kotlin_reference_guide_examples

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

// EXTENSIONS

public fun String.substring(range: IntRange): String = substring(range.start, range.endInclusive + 1)

// typealias

typealias StringPredicate = (String) -> Boolean

// Use object to apply Singleton pattern

object PersonRepository{ fun save(s: String) {} } //...

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Kotlin reference guide


        // Single line comment

        /*
        Multi line
        comment
        */


        // Define Constant:

        val inserted = "Kotlin"
        // System output (to console):
        println("Let’s get started with $inserted")


        // Define Variable:

        var name: String
        name = "Martin"


        // Data Types

        //  Double, Float, Long, Int, Short, Byte

        val myByte: Byte = 100       // -128 to 127

        val myShort: Short = 5000     // -32768 to 32767

        val myInt: Int = 100000     // -2147483648 to 2147483647

        val myLong: Long = 15000000000L  // -9223372036854775807 to 9223372036854775807

        val myFloat: Float = 5.75F

        val myDouble: Double = 19.99

        // with an "e" or "E" to indicate the power of 10

        val myFloat1: Float = 35E3F
        val myDouble2: Double = 12E4

        // conversion methods:

        // toByte(): Byte, toInt(): Int, toLong(): Long

        var x: Int = 5
        var y: Long = x.toLong()
        ++x  // x = x + 1  (before use in expression)
        x++  // x = x + 1  (after use in expression)

        // Char

        val myGrade: Char = 'B'
        // toInt()

        // Boolean

        val isKotlinFun: Boolean = true
        val isFishTasty: Boolean = false

        println(10 > 9) // Returns true, because 10 is greater than 9

        println(x > y) // Returns true, because x is greater than y

        println(10 == 15); // Returns false, because 10 is not equal to 15

        println(x == 7)

        // operators :  ||, &&, and !


        // String

        var hello:String = "Hello World"

        //  raw string that can contain any character without needing to escape special symbols

        hello = """Hello
        World multiline with escape special symbols 
        in it"""

        println(hello)

        println(hello + " " + name)

        println("Variables: $name $hello")

        println(name.plus(hello))

        println("The length of the hello string is: " + hello.length)

        println(hello.uppercase())
        println(hello.lowercase())

        println(hello.compareTo(name))  // Outputs 1 (they are not equal)

        println(hello.indexOf("escape"))


        // Operators

        // Arithmetic Operators: + - * / % ++ --  (x++, ++x)

        // Assignment Operators: = += -= *= /= %=

        // Comparison Operators: < > == != <= >=

        // Logical Operators :  ||  &&  !


        // If ... Else

        if (x > y) {
            println("x is greater than y")
        }

        if (x > y) {
            println("x is greater than y")
        } else {
            println("x is not greater than y")
        }

        if (x < 10) {
            //
        } else if (x >= 10) {
            //
        } else {
            //
        }

        // Ternary operator  (If..Else Expressions)

        val min = if (x < y) x else y

        val time = 20
        val greeting = if (time < 18) {
            "Good day."
        } else {
            "Good evening."
        }

        val greeting2 = if (time < 18) "Good day." else "Good evening."


        // WHEN-STATEMENT (switch)

        val non:String = when (x) {
            0 -> "is zero"
            1 -> "is one"
            2, 3 -> "two or three"
            is Int -> "is Int"
            in 0..100 -> "between 0 and 100"
            else -> "else block"
        }


        // While Loop

        while (x < 15) {
            println(" X: $x")
            x++
        }

        // Do..While Loop

        do {
            println(" Y: $y")
            y++
        }
        while (y < 15)

        // Break and Continue

        while (x < 20) {
            x++
            if (x == 18) {
                break
            }
            println(x)
        }

        while (y < 20) {
            if (y.toInt() == 18) {
                y++
                continue
            }
            println(y)
            y++
        }


        // Array

        val cars = arrayOf("Volvo", "BMW", "Ford", "Mazda")

        println(cars[0])
        println(cars.get(0))

        cars[0] = "Opel"
        cars.set(0, "Opel")

        println(cars.size)

        for (x in cars) {
            println(x)
        }

        if ("Volvo" in cars) {
            println("It exists!")
        } else {
            println("It does not exist.")
        }

        // Array<String> cannot be assigned to a variable of type Array<Any>

        // primitive types exist as IntArray or ShortArray  ...


        // FOR LOOP

        for (c in "charSequence") {
            //
        }

        for (i in "charSequence".indices) {
            println("charSequence"[i])
        }

        for ((i,c) in "charSequence".withIndex()) {
            println("$i: $c")
        }

        (0 .. "charSequence".length-1).forEach {
            print("charSequence"[it])
        }


        // Ranges

        for (chars in 'a'..'x') {
            println(chars)
        }

        for (nums in 5..15) {
            println(nums)
        }


        // Functions

        fun myFunction() {
            println("I just got executed!")
        }

        myFunction()

        fun parametersFunction(fname: String, age: Int) {
            println(fname + " is " + age)
        }

        parametersFunction("John", 35)

        fun returnFunction(x: Int): Int {
            return (x + 5)
        }

        println(returnFunction(6))

        fun shortReturnFunction(x: Int, y: Int) = x + y

        println(shortReturnFunction(5,6))



        // Classes and Objects

        // Classes and Objects

        // Constructor in ()      // open = can be a superclass   // default parameter year = 2022
        open class Car (var brand: String, var model: String, var year: Int = 2022) {

            // Properties with assessors

            var fullName: String
                get() = "$brand $model"
                set(value) {
                    val (first, rest) = value.split(" ", limit = 2)
                    brand = first
                    model = rest
                }


            // main constructor
            init {
                println("new $brand.")
            }

            // Class Functions

            open fun hello() = "Hello, I am $brand"

            fun drive() {
                println("Wrooom!")
            }

            fun speed(maxSpeed: Int) {
                println("Max speed is: " + maxSpeed)
            }
        }

        // Initialize objects :

        val c1 = Car("Ford", "Mustang", 1969)
        val c2 = Car("BMW", "X5", 1999)
        val c3 = Car("Tesla", "Model S", 2020)

        c1.drive()

        // SPECIAL CLASSES

        /*

        data class				Adds standard functionality for toString, equals, hashCode etc.

        sealed class			Restricts class hierarchies to a set of subtypes. Useful with when

        Nested class			Classes can be created in other classes, also known as "inner class"

        enum class				Collect constants that can contain logic

        object declarations		Used to create Singletons of a type

        */

        // Access :

        // Keyword       Functions, properties and classes, objects and interfaces				Class Members

        // public   		 visible everywhere          											visible everywhere if class is accessible

        // private			 visible inside the file only											visible inside the class only

        // protected		 -																		visible in class and subclasses

        // internal 		 visible inside the same module											visible in the same module, if class is accessible

        // open = can be a superclass


        // Inheritance

        //    subclass (child) - the class that inherits from another class
        //    superclass (parent) - the class being inherited from

        // Subclass
        class CombiCar(brand: String, model: String, year: Int): Car(brand, model, year) {

            override fun hello() = "Dzien dobry, jestem $brand"

            fun myFunction() {
                println("asdf")
            }
        }

        // NULL-SAFETY

        // nullable types :

        var b: String? = "couldBeNull"
        b = null //okay

        // 1. Access directly: does not compile, could throw NPE
        // val len = b.length

        //2. Use safe-operator
        val len = b?.length

        //3. Check nullability before accessing
        if(b != null){
            b.length
        }


        // Operators:

/*
Operator			Use case																							Example

!!					Ignore warnings of compiler and overcome null checks. Use cautiously.								val x: String? = "nullable" x!!. length

?:					The elvis operator is used to give an alternative for null results.									val x: String? = "nullable" val len: Int = b?.length ?: 0

as?					A safe cast tries to cast a variable in a type and results in null if the cast is not possible.		val i: Int? = s as? Int

*/

        // EXTENSIONS


        // public fun String.substring(range: IntRange): String = substring(range.start, range.endInclusive + 1)

        //usage
        "myString".substring(0..3)


//        val Int.bd {
//            get() = BigDecimal(this)
//        }
//        val bd: BigDecimal = 5.bd


        // FUNCTION TYPES AND LAMBDAS

        // function types, e.g. "(String) -> Boolean"

        val myFunction: (String) -> Boolean = { s -> s.length > 3 }
        myFunction("HelloWorld")

        // it: implicit name of single parameters

        val myFunction2: (String) -> Boolean = { it.length >3 }
        myFunction2("HelloWorld")

        // For unused parameters, use _

        val myFunction3: (String, Int) -> Boolean = { s, _ -> s.length > 3 }
        myFunction3("HelloWorld", 42)


        //HIGHER-ORDER FUNCTIONS

        fun myHigherOrderFun(iterations: Int, test: (String) -> Boolean){
            (0 until iterations).forEach { println("$it: ${test("myTestString")}")
            } }


        myHigherOrderFun(2, { it.length > 2 })


        //Lambda after closing parentheses
        myHigherOrderFun(2) {
            it.length>2
        }


        // LAMBDA WITH RECEIVER

        // generic type T

        fun <T> T.apply(block: T.() -> Unit): T {
            block()
            return this
        }

        data class GuiContainer(var width: Int = 0, var height: Int = 0, var background: String = "red") {
            fun printMe() = println(this)
        }


        val container = GuiContainer().apply {
            width = 10
            height = 20
            background = "blueish"
            printMe()
        }


        // Kotlin idioms by example

        // 1. Use when as an expression if possible

        fun analyzeType(obj: Any) = when(obj){
            is String -> "is String"
            else -> "no String"
        }

        // 2. Use elvis operator with throw and return to handle nullable values

        class Person(val name: String?, val age: Int?)
        fun process(person: Person) {
            val pName = person.name ?: throw IllegalArgumentException("Name must be provided.")
            println("processing $pName")
            val pAge = person.age ?: return println("$pName is $person.age age years old")
        }

        // 3. Make use of range checks

        fun inLatinAlphabet(char: Char) = char in 'A'..'Z'

        // 4. Prefer default parameters to function overloads

        fun greet(person: Person, printAge: Boolean = false) { println("Hello ${person.name}")
            if (printAge)
                println("${person.name} is ${person.age} years old")
        }

        // 5. Use type aliases for function types

        // typealias StringPredicate = (String) -> Boolean
        val pred: StringPredicate = {it.length > 3}

        // 6. Use data classes for multiple return values

        data class Multi(val s: String, val i: Int)
        fun foo() = Multi("one", 1)
        val (name2, num) = foo()
        val (_, num2) = foo()

        // 7. Prefer extension functions to utility-style functions

        fun Person.greet(printAge: Boolean = false) { println("Hello $name")
            if (printAge)
                println("$name is $age years old")
        }

        // 8. Use apply for object initialization

        data class GuiContainer2(var width: Int = 0, var height: Int = 0, var background: String = "red") {
            fun printMe() = println(this)
            fun main(args: Array<String>) {
                val container = GuiContainer2().apply {
                    width = 10
                    height = 20
                    background = "blueish"
                    printMe()
                }
            }
        }

        // 9. Use compareBy for complex comparisons

        fun sort(persons: List<Person>): List<Person> = persons.sortedWith(compareBy(Person::name,
            Person::age))

        // 10. Use mapNotNull to combine map and filter for non-null values

        fun getPersonNames(persons: List<Person>): List<String> =
            persons.mapNotNull { it.name }


        // 11. Use object to apply Singleton pattern

        //object PersonRepository{ fun save(p: Person){} //...

        //usage
        val p = "Paul"
        PersonRepository.save(p)

        // 12. Do not make use of '!!'

        // Do not use !!, there’s always a better solution
        // person!!.address!!.street!!.length

        // 13. Prefer read-only data structures

        //Whenever possible, do not use mutable Data Structures

        val mutableList: MutableList<Int> = mutableListOf(1, 2, 3)
        mutableList[0] = 0
        val readOnly: List<Int> = listOf(1, 2, 3)
        // readOnly[0] = 0 // Does not compile

        // 14. Use let to execute code if receiver is not null

        fun letPerson(p: Person?) { p?.let {
            println("Person is not null") }
        }


        // Data classes

        data class PersonData(val name: String, var age: Int)
        val mike = PersonData("Mike", 23)

        // Modifier data adds:

        //1. toString that displays all primary constructor properties

        print(mike.toString()) // Person(name=Mike, age=23)

        // 2. equals that compares all primary constructor properties

        print(mike == PersonData("Mike", 23)) // True
        print(mike == PersonData("Mike", 21)) // False

        // 3. hashCode that is based on all primary constructor properties

        val hash = mike.hashCode()
        print(hash == Person("Mike", 23).hashCode()) // True
        print(hash == Person("Mike",21).hashCode()) // False

        // 4 component1, component2 etc. that allows deconstruction
        val (name3, age) = mike
        print("$name3 $age") // Mike 23
        // 5. copy that returns copy of object with concrete properties changed
        val jake = mike.copy(name = "Jake")


        // COLLECTION LITERALS

        listOf(1,2,3,4) // List<Int>
        mutableListOf(1,2,3,4) // MutableList<Int>

        setOf("A", "B", "C") // Set<String>
        mutableSetOf("A", "B", "C") // MutableSet<String>

        arrayOf('a', 'b', 'c') // Array<Char>

        mapOf(1 to "A", 2 to "B") // Map<Int, String>
        mutableMapOf(1 to "A", 2 to "B") // MutableMap<Int, String>

        sequenceOf(4,3,2,1) // Sequence<Int>

        1 to "A" // Pair<Int, String>

        List(4) { it * 2} // List<Int>

        generateSequence(4) { it + 2 } // Sequence<Int>


        // Most important functions for collection processing

        val I = listOf(1,2,3,4)

        // filter - returns only elements matched by predicate
        I.filter { it % 2 == 0 } // 12. 41

        //              map - returns elements after transformation
        I.map { it * 2 } // 12. 4. 6. 81

        // flatMap - returns elements vielded from results of trans.
        I.flatMap { listOf(it, it + 10) } // [1, 11, 2, 12, 3, 13, 4, 141

        // fold/reduce - accumulates elements
        I.fold(0.0) { acc, i -> acc + i } // 10.0
        I.reduce { acc, i -> acc * i } // 24

        // forEach/onEach - perfons an action on every element
        I.forEach { print(it) } // Prints 1234, returns Unit
        I.onEach { print(it) } // Prints 1234, returns [1, 2, 3, 4]

        // min/max/minBy/maxBy
        I.min() // 1, possible because we can compare Int
        I.minBy { -it } // 4
        I.max() // 4, possible because we can compare Int
        I.maxBy { -it } // 1

        // first/firstBy
        I.first() //1
        I.first { it % 2 == 0 } // 2 (first even number)

        // count - count elements matched by predicate
        I.count {it % 2 == 0} //2

        // sorted/sortedBy - returns sorted collection
        listOf(2,3,1,4).sorted() // [1, 2, 3, 4]
        I.sortedBy { it % 2 } // [2, 4, 1, 31
    }
}
