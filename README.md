# Kotlin reference guide examples

[**Author:** Martin Kompan](https://gitlab.com/examples63/mkuseful/-/blob/main/certs/Programming_certificates.md)

Kotlin version: **1.7.20**

## Environment:

Android Studio Dolphin | 2021.3.1 Patch 1

API 29: Android 10.0


## Table of contents

1. [Comments, define constants and variebles, data types, conversions](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L26)
2. [Char, Boolean, String](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L77)
3. [Operators](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L126)
4. [If ... Else, Ternary operator  (If..Else Expressions)](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L137)
5. [WHEN-STATEMENT (switch), While Loop, Do..While Loop, Break and Continue](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L171)
6. [Array](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L218)
7. [FOR LOOP, Ranges](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L245)
8. [Functions](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L275)
9. [Classes and Objects, Constructors, SPECIAL CLASSES, Access](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L303)
10. [Inheritance](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L376)
11. [NULL-SAFETY + operators](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L391)
12. [EXTENSIONS](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L423)
13. [FUNCTION TYPES AND LAMBDAS, HIGHER-ORDER FUNCTIONS](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L438)
14. [LAMBDA WITH RECEIVER](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L472)
15. [Kotlin idioms by example](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L494)
16. [Data classes](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L596)
17. [COLLECTION LITERALS + functions](https://gitlab.com/examples63/kotlin-reference-guide-examples/-/blob/main/app/src/main/java/martin/kompan/demo/kotlin_reference_guide_examples/MainActivity.kt#L625)
